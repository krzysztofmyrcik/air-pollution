import { Component } from '@angular/core';
import { MeasurementsService } from './services/measurements-service/measurements.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'air-pollution';

  constructor(private measurmentService: MeasurementsService) {
    
    // this.measurmentService.getCity()
  }
}

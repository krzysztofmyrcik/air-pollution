import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MeasurementsService } from './services/measurements-service/measurements.service';
import { WikiService } from './services/wiki-service/wiki.service';
import { TableComponent } from './components/table/table.component';
import { InputComponent } from './components/input/input.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    InputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
    
  ],
  providers: [
    MeasurementsService,
    WikiService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }

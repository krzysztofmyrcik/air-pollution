import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MeasurementsService } from 'src/app/services/measurements-service/measurements.service';
import * as _ from 'lodash';
import { ResponseModel } from 'src/app/models/reposne-model';
import { CitiyAirQuality } from 'src/app/models/pollution';
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit {

  @Output() data = new EventEmitter<CitiyAirQuality[]>()

  private citiesNames: string[] = ['Poland', 'Germany', 'Spain', 'France']
  private city: string
  private response: CitiyAirQuality[];
  constructor(private measurementsService: MeasurementsService) {

  }

  ngOnInit() {
  }

  private onSearchChange(name: string): void {
    this.city = name
  }

  private getCity(): void {
    if (this.citiesNames.includes(this.city)) {
      this.measurementsService.getCity(this.getCityCode(this.city))
        .subscribe((response: ResponseModel) => {
          if (response != null && response.results != null && response.results.length > 0) {
            const tempArr: CitiyAirQuality[] = response.results.map((e: CitiyAirQuality) => new CitiyAirQuality(e))
            this.returnPullutedCities(tempArr)
          }
        })
    } else {
      window.alert('akukaracza')
    }
  }

  private returnPullutedCities(response: CitiyAirQuality[]): void {
    this.data.emit(this.measurementsService.getMostPollutedCites(response));
  }

  private getCityCode(city: string): string {
    switch (city) {
      case 'Poland': {
        return 'PL'
      }
      case 'Germany': {
        return 'DE'
      }
      case 'Spain': {
        return 'ES'
      }
      case 'France': {
        return 'FR'
      }
      default:
        console.warn("No such country exists!");
        break;
    }

  }
}

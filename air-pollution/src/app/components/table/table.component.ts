import { Component, OnInit } from '@angular/core';
import { CitiyAirQuality } from 'src/app/models/pollution';
import { WikiService } from 'src/app/services/wiki-service/wiki.service';
import { WikiResponse } from 'src/app/models/wiki-response';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  private cities: CitiyAirQuality[] = [];
  private description: string
  private isActive
  lastElem: HTMLElement;
  constructor(private wikiService: WikiService) { }

  ngOnInit() {
  }

  private createTable(event: CitiyAirQuality[]): void {
    this.lastElem = null
    this.cities = event
  }

  private showDetails(element: string): void {
    this.handleAccordions(element)

    this.wikiService.getDescription(element).subscribe(response => {
      const elem = new WikiResponse(response)
      this.description = elem.extract
    })
    window.document.getElementById(element).classList.toggle('display')
  }

  private handleAccordions(element: string): void {
    if (this.lastElem != null) {
      this.lastElem.classList.toggle('display')
    }
    this.lastElem = window.document.getElementById(element)
  }
}

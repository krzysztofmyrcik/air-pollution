
export class CitiyAirQuality {
  public parameter: string;
  public date: DateClass;
  public value: string;
  public unit: string;
  public location: string;
  public country: string;
  public city: string;
  public sourceName: string;
  public averagingPeriod: AveragingPeriod;
  public coordinates: Coordinates;
  public attribution: Attribution[];

  constructor(obj: any) {
    this.fromJSON(obj)
  }

  public fromJSON(obj: any) {
    if (obj != null) {
      this.parameter = obj.parameter
      this.date = obj.date != null ? new DateClass(obj.date) : null
      this.value = obj.value
      this.unit = obj.unit
      this.location = obj.location
      this.country = obj.country
      this.city = obj.city
      this.sourceName = obj.sourceName
      this.averagingPeriod = obj.averagingPeriod != null ? new AveragingPeriod(obj.averagingPeriod) : null
      this.coordinates = obj.coordinates != null ? new Coordinates(obj.coordinates) : null
      this.attribution = obj.attribution != null ? obj.attribution.map((e: any) => new Attribution(e)) : null
    }
  }
}

export class Attribution {
  public name: string;
  public url: string;

  constructor(obj: any) {
    this.fromJSON(obj)
  }

  public fromJSON(obj: any) {
    if (obj != null) {
      this.name = obj.name
      this.url = obj.url
    }
  }

}

export class AveragingPeriod {
  public unit: string;
  public value: number;

  constructor(obj: any) {
    this.fromJSON(obj)
  }

  public fromJSON(obj: any) {
    if (obj != null) {
      this.unit = obj.unit
      this.value = obj.value
    }
  }

}

export class Coordinates {
  public latitude: number;
  public longitude: number;

  constructor(obj: any) {
    this.fromJSON(obj)
  }

  public fromJSON(obj: any) {
    if (obj != null) {
      this.latitude = obj.latitude
      this.longitude = obj.longitude
    }
  }

}

export class DateClass {
  public utc: string;
  public local: string;

  constructor(obj) {
    this.fromJSON(obj)
  }

  public fromJSON(obj: any) {
    if (obj != null) {
      this.utc = obj.utc
      this.local = obj.local
    }
  }

}
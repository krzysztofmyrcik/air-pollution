
export class WikiResponse {
    public type: string;
    public title: string;
    public displaytitle: string;
    public namespace: Namespace;
    public wikibase_item: string;
    public titles: Titles;
    public pageid: number;
    public thumbnail: OriginalImage;
    public originalimage: OriginalImage;
    public lang: string;
    public dir: string;
    public revision: string;
    public tid: string;
    public timestamp: string;
    public description: string;
    public coordinates: Coordinates;
    public content_urls: ContentUrls;
    public api_urls: APIUrls;
    public extract: string;
    public extract_html: string;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.type = obj.type
            this.title = obj.title
            this.displaytitle = obj.displaytitle
            this.namespace = obj.namespace
            this.wikibase_item = obj.wikibase_item
            this.titles = obj.titles != null ? new Titles(obj.titles) : null
            this.pageid = obj.pageid
            this.thumbnail = obj.thumbnail
            this.originalimage = obj.originalimage != null ? new OriginalImage(obj.originalimage) : null
            this.lang = obj.lang
            this.dir = obj.dir
            this.revision = obj.revision
            this.tid = obj.tid
            this.timestamp = obj.timestamp
            this.description = obj.description
            this.coordinates = obj.coordinates != null ? new Coordinates(obj.coordinates) : null
            this.content_urls = obj.content_urls != null ? new ContentUrls(obj.content_urls) : null
            this.api_urls = obj.api_urls != null ? new APIUrls(obj.api_urls) : null
            this.extract = obj.extract
            this.extract_html = obj.extract_html
        }
    }

}

export class APIUrls {
    public summary: string;
    public metadata: string;
    public references: string;
    public media: string;
    public edit_html: string;
    public talk_page_html: string;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.summary = obj.summary
            this.metadata = obj.metadata
            this.references = obj.references
            this.media = obj.media
            this.edit_html = obj.edit_html
            this.talk_page_html = obj.talk_page_html
        }
    }
}

export class ContentUrls {
    public desktop: Desktop;
    public mobile: Desktop;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.desktop = obj.desktop != null ? new Desktop(obj.desktop) : null
            this.mobile = obj.mobile != null ? new Desktop(obj.mobile) : null
        }
    }
}

export class Desktop {
    public page: string;
    public revisions: string;
    public edit: string;
    public talk: string;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.page = obj.page
            this.revisions = obj.revisions
            this.edit = obj.edit
            this.talk = obj.talk
        }
    }
}

export class Coordinates {
    public lat: number;
    public lon: number;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.lat = obj.lat
            this.lon = obj.lon
        }
    }
}

export class Namespace {
    public id: number;
    public text: string;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.id = obj.id
            this.text = obj.text
        }
    }
}

export class OriginalImage {
    public source: string;
    public width: number;
    public height: number;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.source = obj.source
            this.width = obj.width
            this.height = obj.height
        }
    }
}

export class Titles {
    public canonical: string;
    public normalized: string;
    public display: string;

    constructor(obj: any) {
        this.fromJSON(obj)
    }

    private fromJSON(obj: any) {
        if (obj) {
            this.canonical = obj.canonical
            this.normalized = obj.normalized
            this.display = obj.display
        }
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CitiyAirQuality } from 'src/app/models/pollution';
import { ResponseModel } from 'src/app/models/reposne-model';
import * as _ from 'lodash'
import { Observable } from 'rxjs';

const measurementsUtl: string = 'https://api.openaq.org/v1/measurements'

@Injectable({
  providedIn: 'root'
})

export class MeasurementsService {


  constructor(private httpClient: HttpClient) {

  }

  private getBasicResponse(options: string, parametr: string = 'pm25', records: number = 500): Observable<ResponseModel> {
    if (_.size(options) > 0) {
      return this.httpClient.get<ResponseModel>(measurementsUtl + '?country=' + options + '&order_by=value&sort=desc&parameter=' + parametr + '&limit=' + records.toString() + '')
    } else {

      return this.httpClient.get<ResponseModel>(measurementsUtl)
    }
  }

  public getCity(options: string): Observable<ResponseModel> {
    return this.getBasicResponse(options)
  }

  public getMostPollutedCites(cities: CitiyAirQuality[]): CitiyAirQuality[] {
    const uniqCities: Set<string> = new Set(cities.map(e => e.city))
    const topCities: CitiyAirQuality[] = []
    for (let city of uniqCities) {
      const element: CitiyAirQuality = _.find(cities, e => city === e.city)
      if (element != null && topCities.length < 10) {
        topCities.push(element)
      } else {
        return topCities
      }
    }

  }
}

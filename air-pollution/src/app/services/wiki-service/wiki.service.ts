import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WikiResponse } from 'src/app/models/wiki-response';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WikiService {
  private basicUrl: string = `https://en.wikipedia.org/api/rest_v1/page/summary/`
  constructor(private httpClient: HttpClient) {

  }

  public getDescription(city: string):Observable<WikiResponse> {
    city = this.removeLathinChars(city)

    return this.httpClient.get<WikiResponse>(this.basicUrl + city)
  }

  private removeLathinChars(str: string): string {
    return str.replace(/ą/g, 'a').replace(/Ą/g, 'A')
      .replace(/ć/g, 'c').replace(/Ć/g, 'C')
      .replace(/ę/g, 'e').replace(/Ę/g, 'E')
      .replace(/ł/g, 'l').replace(/Ł/g, 'L')
      .replace(/ń/g, 'n').replace(/Ń/g, 'N')
      .replace(/ó/g, 'o').replace(/Ó/g, 'O')
      .replace(/ś/g, 's').replace(/Ś/g, 'S')
      .replace(/ż/g, 'z').replace(/Ż/g, 'Z')
      .replace(/ź/g, 'z').replace(/Ź/g, 'Z')
      .replace(' ', '%20')
  }

}
